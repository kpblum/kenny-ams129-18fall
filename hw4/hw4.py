def right_justify(s):
    print('%70s' % (s))
    return

def count_char(s, c):
    n = 0
    for character in s:
        if(character == c):
            n += 1
    print('Total number of %s in the given string: %s' % (c, n))
    return

def cumulative_sum(L):
    listsum = 0
    newList = []
    for index, value in enumerate(L):
        listsum += value
        newList.append(listsum)
    return newList

def check_palindrome(s):
    reverse = s[::-1]
    if(s == reverse):
        return True
    else:
        return False

def main():
    right_justify('Kenneth Paul Blum')

    lorem = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla iaculis tellus id mollis scelerisque. In consequat nec tellus lacinia iaculis. Mauris auctor volutpat aliquam. Nulla dignissim arcu placerat tellus pretium, vel venenatis sem porta. Donec interdum tincidunt mi, et convallis velit aliquet eget. Nam id rutrum felis. Fusce vel fermentum justo. Pellentesque faucibus orci at velit vehicula dignissim. Duis faucibus dapibus malesuada. Pellentesque iaculis tristique vestibulum. Sed egestas nisl non augue imperdiet mattis. Nunc vitae purus lectus. Vestibulum mi turpis, volutpat vel odio quis, hendrerit suscipit ligula. Nunc in massa diam. Suspendisse aliquam quam et ex egestas vestibulum vitae id libero. Cras molestie consectetur condimentum.'
    count_char(lorem, 't')

    result = cumulative_sum([1, 2, 3, 4, 5, 6, 7, 8, 9, 10])
    print(result)

    words_list = ['noon', 'madam', 'ams129', 'redivider', 'numpy', 'bob', 'racecar', 'youngjun']
    for word in words_list:
        if(check_palindrome(word)):
            print('%s is palindrome!' % (word))

if __name__ == "__main__":
    main()
