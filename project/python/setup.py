import numpy as np
import matplotlib.pyplot as plt
import os.path

# this function will parse an output file and input only the float values into a numpy array
def create_array(address, N):
    coordarray = np.zeros([2, N])
    file = open(address, 'r')
    arraysize = 0
    numstring = ""
    for line in file:
        for char in line:
            if (char == '-' or char == '.' or char.isdigit()):
                numstring = numstring + char
            if ((char == ' ' or char == '\n') and numstring != "" and arraysize != (2 * N)):
                float(numstring)
                if (arraysize % 2 == 0):
                    val = int(arraysize/2)
                    coordarray[0][val] = numstring
                else:
                    val = int((arraysize - 1)/2)
                    coordarray[1][val] = numstring
                arraysize += 1
                numstring = ""
    file.close()
    return coordarray

# this function will compute the exact solution array using the indexes of the approximate solution array
def exact_solution(array, N):
    coordarray = np.zeros([2, N])
    solutionrow = np.zeros(N)
    coordarray[0,:] = array[0,:]
    for i in range(N):
        solutionrow[i] = -1 * (np.sqrt(2 * np.log(array[0,i]**2 + 1) + 4))
    coordarray[1,:] = solutionrow
    return coordarray

def main():

    # initializing fileaddress and figures
    figures = np.zeros(4)
    grid_value = 1
    val = 0
    # this loop will search for text files of the form output_N.txt,
    # thus any values of N can be input into this program
    while (val != 4):
        fileaddress = "../fortran/output_" + str(grid_value) + ".txt"
        if (os.path.isfile(fileaddress)):
            figures[val] = grid_value
            val += 1
        grid_value += 1
    figures = figures.astype(int)
    
    # for each figure graph to be created...
    for index1, value1 in enumerate(figures):
        coordlist_approx = None
        coordlist_exact = None
        graphname = ""
        indexes = list()
        c_a_values = list()
        c_e_values = list()
        errors = list()
        totalerror = 0

        # initialize coordlist_approx, coordlist_exact, and graphname
        fileaddress = "../fortran/output_" + str(value1) + ".txt"
        coordlist_approx = create_array(fileaddress, value1)
        coordlist_exact = exact_solution(coordlist_approx, value1)
        graphname = "result_" + str(value1) + ".png"

        # initialize indexes, c_a_values, c_e_values
        for index2 in coordlist_approx[0,:]:
            indexes.append(index2)
        for value2 in coordlist_approx[1,:]:
            c_a_values.append(value2)
        for value3 in coordlist_exact[1,:]:
            c_e_values.append(value3)

        # initialize errors and totalerror
        for index in range(value1):
            errors.append(abs(c_e_values[index] - c_a_values[index]))
        for value in errors:
            totalerror += value

        # plot figures
        plt.gcf().clear()   # without this function, previously created graphs will also be plotted
        plt.plot(indexes, c_a_values, 'ro', markersize=3)
        plt.plot(indexes, c_a_values, 'r--', linewidth=1)
        plt.plot(indexes, c_e_values, 'b', linewidth=1)
        plt.title("Total error: %s" % (totalerror))
        plt.xlabel("T Axis")
        plt.ylabel("Y Axis")
        plt.grid(True)
        plt.savefig(graphname)
        print("%s created!" % (graphname))
        plt.show()

    print("Program completed")

if __name__ == "__main__":
    main()
