subroutine calc_basel(threshold)
  use param
  implicit none
  real :: threshold, solution, error, numsum = 0.
  integer :: i = 1
  open(unit=20, file="results.txt")
  solution = (pi**2) / 6.
  do
    numsum = numsum + (1./(i**2))
    error = solution - numsum
    if (error < threshold) then
      exit
    else if (mod(i, 1000) == 0) then
      write(20,*) "i = ", i, "     numerical solution = ", numsum, "     error = ", error
    end if
    i = i + 1
  end do
  close(20)
  print *, "---------------------------------------"
  print *, "Total number of cycles: ", i
  print *, "Numerical Solution: ", numsum
  print *, "Error: ", error
end subroutine calc_basel
