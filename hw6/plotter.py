import numpy as np
import matplotlib.pyplot as plt

def word_count(address):
    letters = dict()                    # using default constructor to create empty dictionary
    for index in range(97, 123):        # using ASCII values of a-z to create dictionary keys
        letters[chr(index)] = 0         # sets all keys = 0

    file = open(address, 'r')           # open file as read-only
    for line in file:                   # reads line by line
        for char in line:               # reads character by character in current line
            if (char == '\n'): break    # if character is newline escape sequence, break from character loop and iterate to next line
            else:
                letters[char] += 1      # otherwise increment value for character key by 1
    file.close()

    for key, value in sorted(letters.items()):
        print('\'%s\': %s' % (key, value))          # outputs dictionary sorted alphabetically by keys
    return letters                                  # return completed dictionary

def main():
    file_path = './words.txt'                       # file_path set to local address of words.txt
    hist = word_count(file_path)                    # create dictionary hist by passing file_path to word_count function
    keys = list(hist)                               # creates list of keys
    values = np.array(list(hist.values()))          # creates numpy array of values
    values = sorted(values)                         # sorts array
    x = np.arange(len(keys))                        # creates x-axis with number of elements in keys (26 elements)
    for key, value in sorted(hist.items()):
        plt.bar(key, value, width=0.75, color='#2A77AE')            # plots blue bars (exact hex color and width as image provided)
        plt.plot(key, value, 'ro', markersize=3)                    # plots red circle points with markersize 3 (seemed closest to image)
        plt.plot(hist.keys(), hist.values(), 'r', linewidth=1)      # plots red lines with linewidth 1 (seemed closest to image)

    plt.xticks(keys)                                # x-ticks set to keys (a - z)
    plt.xlabel('Characters')                        # labels x-axis as 'Characters'
    plt.ylabel('Frequency')                         # labels y-axis as 'Frequency'
    plt.grid(True)                                  # enables grid
    plt.savefig('results.png', dpi = 300)           # outputs graph as 'results.png' with dpi 300 (makes for a clearer image)
    print('Graph created as results.png')           

if __name__ == "__main__":
    main()
