def newtons(f, df, initial_guess, threshold):
    index = 1
    L_x = [initial_guess]
    L_dx = [initial_guess]
    print("  n|                     x|                    dx")
    print("-------------------------------------------------")
    print("%3s|%22s|%22s" % (0, L_x[0], L_dx[0]))
    while True:
        L_x.append(L_x[index - 1] - (f(L_x[index - 1]) / df(L_x[index - 1])))
        L_dx.append(abs(L_x[index - 1] - L_x[index]))
        print("%3s|%22s|%22s" % (index, '{0:.18f}'.format(L_x[index]),'{0:.18f}'.format(L_dx[index])))
        if (L_dx[index] <= threshold):
            break
        index += 1
